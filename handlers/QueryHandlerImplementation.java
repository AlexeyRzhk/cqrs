package handlers;

import queries.Query;

public class QueryHandlerImplementation implements QueryHandler {
    @Override
    public void ask(Query query) {
        query.ask();
    }
}
