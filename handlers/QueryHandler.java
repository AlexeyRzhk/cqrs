package handlers;

import queries.Query;

public interface QueryHandler {

    public void ask(Query query);
}
