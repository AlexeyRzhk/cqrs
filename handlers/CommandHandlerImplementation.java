package handlers;

import commands.Command;

public class CommandHandlerImplementation implements CommandHandler {
    @Override
    public void Handle(Command command) {
        command.execute();
    }
}
