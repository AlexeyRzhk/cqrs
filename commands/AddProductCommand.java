package commands;

import entityes.Products;
import storages.Basket;

public class AddProductCommand implements Command {

    private String productId;
    private Integer amount;

    @Override
    public void execute() {
        Products product = Products.valueOf(productId);
        Basket.addProduct(product, amount);
    }

    public AddProductCommand(String productId, Integer amount) {
        this.productId = productId;
        this.amount = amount;
    }
}
