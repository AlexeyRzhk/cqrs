package commands;

import entityes.Products;
import storages.Basket;

public class DeleteProductCommand implements Command {

    private String productId;
    private Integer amount;

    @Override
    public void execute() {
        Products product = Products.valueOf(productId);
        Basket.deleteProduct(product, amount);
    }

    public DeleteProductCommand(String productId, Integer amount) {
        this.productId = productId;
        this.amount = amount;
    }
}
