import commands.AddProductCommand;
import commands.DeleteProductCommand;
import entityes.Products;
import handlers.CommandHandler;
import handlers.CommandHandlerImplementation;
import handlers.QueryHandler;
import handlers.QueryHandlerImplementation;
import queries.ProductQuery;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private static CommandHandler commandHandler = new CommandHandlerImplementation();
    private static QueryHandler queryHandler = new QueryHandlerImplementation();

    public static void main(String[] args) {
        int i = 0;
        while(true) {
            try {
                System.out.println("чтобы добавить продукт в корзину нажмите 1");
                System.out.println("чтобы убрать продукты из корзины нажмите 2");
                System.out.println("чтобы посмотреть что в корзине нажмите 3");
                System.out.println("чтобы закончить работу с корзиной нажмите 4");
                i = Integer.parseInt(reader.readLine());
                if(i == 1){
                    System.out.println("выберите продукт, для этого введите его название на английском");
                    System.out.println("вода - water");
                    System.out.println("хлеб - bread");
                    System.out.println("молоко - milk");
                    String productId = reader.readLine();
                    productId = productId.toUpperCase();
                    System.out.println("введите количество товара");
                    int amount = Integer.parseInt(reader.readLine());
                    commandHandler.Handle(new AddProductCommand(productId, amount));
                }
                if(i == 2){
                    System.out.println("выберите продукт, для этого введите его название на английском");
                    System.out.println("вода - WATER");
                    System.out.println("хлеб - BREAD");
                    System.out.println("молоко - MILK");
                    String productId = reader.readLine();
                    productId = productId.toUpperCase();
                    System.out.println("введите количество товара");
                    int amount = Integer.parseInt(reader.readLine());
                    commandHandler.Handle(new DeleteProductCommand(productId, amount));
                }
                if(i == 3){
                    for(Products product: Products.values()){
                        queryHandler.ask(new ProductQuery(product.getId()));
                    }
                }
                if(i == 4){
                    break;
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
