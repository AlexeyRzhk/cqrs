package storages;

import entityes.Products;
import events.ProductAddEvent;
import events.ProductDeleteEvent;

import java.util.ArrayList;

public class EventStorage {

    private static ArrayList<Products> products = new ArrayList<>();

    private static ArrayList<ProductAddEvent> addEvents = new ArrayList<>();

    private static ArrayList<ProductDeleteEvent> deleteEvents = new ArrayList<>();

    public static void addEvent(ProductAddEvent event){
        addEvents.add(event);

    }

    public static void addEvent(ProductDeleteEvent event){
        deleteEvents.add(event);
    }

    public static Integer getAmount(String id){
        int amount = 0;
        for(ProductAddEvent event: addEvents){
            if(event.getProductId().equals(id)) {
                amount += event.getAmount();
            }
        }
        for(ProductDeleteEvent event: deleteEvents){
            if(event.getProductId().equals(id)) {
                amount -= event.getAmount();
            }
        }
        if(amount <= 0){
            deleteElement(id);
            amount = 0;
        }
        return amount;
    }


    private static void deleteElement(String id){
        for(int i = 0;i < addEvents.size();i++){
            if(addEvents.get(i).getProductId().equals(id)){
                addEvents.remove(i);
                i--;
            }
        }
        for(int i = 0;i < deleteEvents.size();i++){
            if(deleteEvents.get(i).getProductId().equals(id)){
                deleteEvents.remove(i);
                i--;
            }
        }
        for(int i = 0;i < products.size();i++){
            if (products.get(i).getId().equals(id)){
                products.remove(i);
                break;
            }
        }
    }




}
