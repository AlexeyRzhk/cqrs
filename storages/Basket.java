package storages;

import entityes.Products;
import events.ProductAddEvent;
import events.ProductDeleteEvent;

import java.util.HashMap;

public class Basket {

    private static HashMap<Products, Integer> storage = new HashMap<>();



    public static void addProduct(Products product, int amount){
        storage.put(product, storage.getOrDefault(product, 0) + amount);
        EventStorage.addEvent(new ProductAddEvent(product.getId(), product.getTitle(), amount));
    }

    public static void deleteProduct(Products product, int amount){
        if(storage.get(product) > amount){
            storage.put(product, storage.get(product) - amount);
        }
        else{
            storage.put(product, 0);
        }
        EventStorage.addEvent(new ProductDeleteEvent(product.getId(), product.getTitle(), amount));
    }

}
