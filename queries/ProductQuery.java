package queries;


import storages.EventStorage;

public class ProductQuery implements Query {

    private String productId;

    public void ask(){

        System.out.println(productId + " " + EventStorage.getAmount(productId));
    }

    public ProductQuery(String productId) {
        this.productId = productId;
    }
}
