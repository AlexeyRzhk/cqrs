package entityes;

public enum Products {
    MILK("milk", "milk"),
    BREAD("bread", "bread"),
    WATER("water", "water");

    Products(String id, String title) {
        this.id = id;
        this.title = title;
    }

    String id;
    String title;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Products getProductById(String id){
        for (Products product: Products.values()) {
            if (product.id.equals(id)) {
                return product;
            }
        }
        return null;
    }
}
