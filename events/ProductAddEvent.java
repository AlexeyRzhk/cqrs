package events;

public class ProductAddEvent {

    private String productId;
    private String title;
    private Integer amount;

    public ProductAddEvent(String productId, String title, Integer amount) {
        this.productId = productId;
        this.title = title;
        this.amount = amount;
    }

    public String getProductId() {
        return productId;
    }

    public String getTitle() {
        return title;
    }

    public Integer getAmount() {
        return amount;
    }
}
