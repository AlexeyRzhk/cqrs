package events;

public class ProductDeleteEvent {

    private String productId;
    private String title;
    private Integer amount;

    public ProductDeleteEvent(String productId, String title, Integer amount) {
        this.productId = productId;
        this.title = title;
        this.amount = amount;
    }

    public String getProductId() {
        return productId;
    }

    public String getTitle() {
        return title;
    }

    public Integer getAmount() {
        return amount;
    }
}
